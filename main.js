function createUUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}


function selectByTagOnText(event, id) {
    const cellText = document.getSelection();
    if (!cellText.isCollapsed)
        return true;
    console.log('cellText', cellText)
    function findPartByIdRec(part, id) {
        if (part.id == id)
            return part;
        for (p of part.subparts) {
            let x = findPartByIdRec(p, id)
            if (x != null)
                return x;
        }
        return null;
    }
    let part = findPartByIdRec(mainPart, id);
    console.assert(part != null);
    console.log('selectByTag', part)
    event.preventDefault();
    event.stopPropagation();
    if (mainManager.selectedPart != part)
        mainManager.selectToggle(part);
    return false;
}
