

class Manager {

    selectedPart = null;

    deselect(part) {
        // console.log('deselect', part)
        part.uiElement.toggleClass('part-selected')
        this.selectedPart = null;
        this.renderText();
    }

    // calculateTagElements(range1, tag1, range2, tag2) {
    //     let tagElements = [];
    //     if (range1[0] <= range2[0]) {
    //         tagElements.push([range1[0], `<${tag1}>`])
    //         if (range1[1] <= range2[0]) {
    //             tagElements.push([range1[1], `</${tag1}>`])
    //             tagElements.push([range2[0], `<${tag2}>`])
    //             tagElements.push([range2[1], `</${tag2}>`])
    //         } else {
    //             tagElements.push([range2[0], `<${tag2}>`])
    //             if (range2[1] <= range1[1]) {
    //                 tagElements.push([range2[1], `</${tag2}>`])
    //                 tagElements.push([range1[1], `</${tag1}>`])
    //             } else {
    //                 tagElements.push([range1[1], `</${tag2}>`]) // for ballance
    //                 tagElements.push([range1[1], `</${tag1}>`])
    //                 tagElements.push([range1[1], `<${tag2}>`]) // for ballance
    //                 tagElements.push([range2[1], `</${tag2}>`])
    //             }
    //         }
    //     } else {
    //         return this.calculateTagElements(range2, tag2, range1, tag1);
    //     }
    //     return tagElements;
    // }

    //render open tag
    rOT(rangeTag) {
        // console.log('rOT', rangeTag.tag)
        let clickable = rangeTag.clickable ? `onclick="selectByTagOnText(event, '${rangeTag.part.id}')"` : ''
        return `<${rangeTag.tag} ${clickable}>`
    }
    rCT(rangeTag) {
        return `</${rangeTag.tag}>`
    }

    calculateTagElements2(rangeTags) {
        let rangeTagEvents = rangeTags.map(function(rt) { return {loc: rt.range[0], tag: rt.tag, type: 'open', len: rt.range[1] - rt.range[0], part: rt.part, clickable: rt.clickable};} ).
            concat(rangeTags.map(function(rt) { return {loc: rt.range[1], tag: rt.tag, type: 'close', len: rt.range[1] - rt.range[0], part: rt.part, clickable: rt.clickable};} ));
        rangeTagEvents.sort(function(a, b) {
            if (a.loc != b.loc)
                return a.loc - b.loc;
            if (a.type != b.type)
                if (a.type == 'close')
                    return -1;
                else
                    return +1;
            return -(a.len - b.len);
        })
        // console.log('rangeTagEvents', rangeTagEvents)

        let tagElements = [];
        let openTags = [];
        for (let i=0; i<rangeTagEvents.length; i++) {
            // console.log('calculateTagElements2', rangeTagEvents[i], openTags)
            if (rangeTagEvents[i].type == 'open')  {
                tagElements.push([rangeTagEvents[i].loc, this.rOT(rangeTagEvents[i]) ])
                openTags.push(rangeTagEvents[i])
            } else { //close
                // console.log('calculateTagElements2 close', openTags)
                let tempTags = [];
                while (openTags.length > 0 && openTags.slice(-1)[0].tag != rangeTagEvents[i].tag) {
                    // console.log('calculateTagElements2', 'pop b', openTags.slice(-1)[0])
                    tagElements.push([rangeTagEvents[i].loc, this.rCT(openTags.slice(-1)[0])])
                    tempTags.push(openTags.pop())
                }
                console.assert(openTags.length > 0)
                console.log(openTags, openTags.slice(-1)[0].tag, rangeTagEvents[i].tag);
                console.assert(openTags.slice(-1)[0].tag == rangeTagEvents[i].tag);
                openTags.pop()
                tagElements.push([rangeTagEvents[i].loc, this.rCT(rangeTagEvents[i])])
                while (tempTags.length > 0) {
                    // console.log('calculateTagElements2', 'push b', tempTags.slice(-1)[0])
                    tagElements.push([rangeTagEvents[i].loc, this.rOT(tempTags.slice(-1)[0]) ])
                    openTags.push(tempTags.pop())
                }
            }
        }
        // console.log('tagElements', tagElements)
        return tagElements;
    }

    renderText() {
        let part = this.selectedPart;
        if (part == null) {
            $('#quran-text').html(sureText);
            return;
        }

        // if (part.parentPart != null && part.parentPart.range != null && part.range != null) {
        //     tagElements = this.calculateTagElements(part.parentPart.range, 'range-parent', part.range, 'range-current');
        // } else {
        //     if (part.range != null)
        //         tagElements = [[part.range[0], '<range-current>'], [part.range[1], '</range-current>']]
        //     if (part.parentPart != null && part.parentPart.range != null)
        //         tagElements = [[part.parentPart.range[0], '<range-parent>'], [part.parentPart.range[1], '</range-parent>']]
        // }
        let rangeTags = [];
        if (part.parentPart != null && part.parentPart.range != null) {
            rangeTags.push({range: part.parentPart.range, tag: 'range-parent', part: part.parentPart, clickable: false})
        }
        if (part.range != null) {
            rangeTags.push({range: part.range, tag: 'range-current', part: part, clickable: false})
        }
        if (part.parentPart != null) {
            let sr = part.parentPart.subparts.
                filter((p) => p.range != null && p != part).
                map((p) => {return {range: p.range, tag: 'range-sibling', part: p, clickable: true};});
            rangeTags = rangeTags.concat(sr);
        }
        let tagElements = this.calculateTagElements2(rangeTags);
        // console.log('select', tagElements)
        let ret = sureText.substring(0, tagElements.length > 0 ? tagElements[0][0] : sureText.length);
        // console.log('select', ret)
        for (let i=0; i<tagElements.length; i++) {
            // console.log('select', ret, i, i+1)
            ret += tagElements[i][1];
            // console.log('select', ret)
            ret += sureText.substring(tagElements[i][0], (i+1 < tagElements.length) ? tagElements[i+1][0] : sureText.length);
            // console.log('select', ret)
        }
        
        $('#quran-text').html(ret);
    }

    select(part) {
        console.assert(this.selectedPart == null)
        // console.log('select', part, part.uiElement.get()[0])
        part.uiElement.addClass('part-selected')
        this.selectedPart = part;
        this.renderText();
    }

    selectToggle(part) {
        console.log('selectToggle', part)
        if (this.selectedPart == part) {
            this.deselect(part);
            return;
        }
        if (this.selectedPart != null) {
            this.deselect(this.selectedPart);
        }
        this.select(part)
    }

    getBaseOffset(node, offset) {
        let mainParent = $('#quran-text').get()[0];
        // console.log('getBaseOffset', node)
        while (node != mainParent && node.previousSibling == null) {
            node = node.parentNode;
            // console.log('getBaseOffset move to parent', node)
        }
        if (node != mainParent)
            node = node.previousSibling;
        while (node != mainParent) {
            // console.log('getBaseOffset', node)
            offset += node.textContent.length;
            while (node != mainParent && node.previousSibling == null) {
                node = node.parentNode;
                // console.log('getBaseOffset move to parent', node)
            }
            if (node != mainParent)
                node = node.previousSibling;
        }
        return offset;
    }

    isChildOfMainParent(node) {
        let mainParent = $('#quran-text').get()[0];
        while (node != null) {
            node = node.parentNode       
            if (node == mainParent)
                return true;
        }
        return false;
    }

    applySelection(part = null) {
        let quranTextNode = $('#quran-text').get()[0];
        let s = window.getSelection();
        if (part == null)
            part = this.selectedPart;
        if (part == null) {
            alert('بخشی انتخاب نشده.')
            return;
        }
        if (s.isCollapsed) {
            part.range = null;
        } else {
            console.log('focusBaseOffset')
            let focusBaseOffset = this.isChildOfMainParent(s.focusNode) ? this.getBaseOffset(s.focusNode, s.focusOffset) : sureText.length;
            console.log('anchorBaseOffset')
            let anchorBaseOffset = this.getBaseOffset(s.anchorNode, s.anchorOffset);
            part.range = [Math.min(focusBaseOffset, anchorBaseOffset), Math.max(focusBaseOffset, anchorBaseOffset)]
            // console.log('applySelection', part.range, anchorBaseOffset, focusBaseOffset)
        }
        part.checkRecursive();
        this.renderText();
        // this.deselect(part)
        // this.select(part)
        // console.log(quranTextNode, s.anchorNode, s.focusNode)
        // if (s.anchorNode != quranTextNode || s.focusNode  != quranTextNode) {
        //     alert('Selection not in the quran text')
        // }
    }

}

class Type {

    availablePatterns() {
        throw "Abstract method not implemented";
    }

}

class Pattern {
    reapply(part) {
        throw "Abstract method not implemented";
    }
    check(part) {
        throw "Abstract method not implemented";
    }
}

class PartFillingStatus {
    static Init = new PartFillingStatus("init", "&#9998;", 2)
    static Done = new PartFillingStatus("done", "&#10003;", 0)
    static Warning = new PartFillingStatus("warning", "&#9888;", 3)
    static Error = new PartFillingStatus("error", "&#9940;", 4)
    static Info = new PartFillingStatus("info", "&#10071;", 1)
  
    constructor(name, sign, priority) {
      this.name = name;
      this.sign = sign;
      this.priority = priority;
    }

    render() {
        return `<span title="${this.name}">${this.sign}</span>`;
    }

    max(s) {
        return (this.priority > s.priority) ? this : s;
    }
}

class PartComment {
    type = PartFillingStatus.Init;
    text = ""
    constructor(type, text) {
        this.type = type;
        this.text = text;
    }

    render() {
        // return '' + this.type.render() + ' ' + this.text
        return `<li style="list-style-type: '${this.type.sign}';">${this.text}`
    }
}

class Part {
    pattern;
    type;
    range = null;
    subparts = [];
    parentPart = null;
    status = PartFillingStatus.Init;
    comments = [];
    id;
    manager;

    //ui elements:
    uiElement;
    info;
    container;
    constructor(manager, type, parentPart) {
        this.manager = manager;
        this.parentPart = parentPart;
        this.type = type;
        this.id = createUUID();
    }

    checkRecursive() {
        this.comments = [];
        this.subparts.forEach(p => {
            p.checkRecursive();
        });
        if (this.pattern != null) {
            this.pattern.check(this);
        } else {
            console.log('checkRecursive', this.pattern, this.type.availablePatterns().length)
            if (this.type.availablePatterns().length > 0)
                this.comments.push(new PartComment(PartFillingStatus.Warning, "نخست باید الگویی انتخاب شود."));
        }
        let maxStatusType = this.comments.reduce((total, c) => total.max(c.type), PartFillingStatus.Done)
        if (maxStatusType == PartFillingStatus.Done)
            this.info.css('color', 'gray')
        else if (maxStatusType == PartFillingStatus.Info || maxStatusType == PartFillingStatus.Init)
            this.info.css('color', 'blue')
        else if (maxStatusType == PartFillingStatus.Warning)
            this.info.css('color', 'darkorange')
        else if (maxStatusType == PartFillingStatus.Error)
            this.info.css('color', 'red')

    }

    erase(p) {
        this.subparts = this.subparts.filter((v) => { return v != p; })
        let x = false;
        for (let pp = this.manager.selectedPart; pp != null; pp = pp.parentPart)
            x = x || p == pp;
        if (x)
            this.manager.deselect(p)
        p.uiElement.remove()
        this.checkRecursive();
    }

    //actions from outside (user)
    reapplyPattern() {
        this.pattern.reapply(this.subparts)
    }

    checkPattern() {
        this.pattern.check(this.subparts)
    }

    clear() {
        this.subparts = [];
    }

    addSubpart(subpart, loc) {
        this.subparts.splice(loc, 0, subpart)
    }

    //ui
    render() {
        let part = this;
        this.uiElement = $('<div class="part" id="' + this.id + '">' + this.type.title() + ': </div>').click((event) => {
            // console.log('uiElement.click', part, event.target, event.target == part.uiElement.get()[0])
            if (part.uiElement.get().length > 0 && event.target == part.uiElement.get()[0])
                this.manager.selectToggle(part);
            return false;
        });
        if (this.type.availablePatterns().length > 0) {
            let pat = $('<select/>')
            $('<option/>').val('null').text('').appendTo(pat)
            // console.log(this.type, this)
            this.type.availablePatterns().forEach((p) => {
                $('<option/>').val(p.name).text(p.title()).appendTo(pat)
            })
            pat.change(function() {
                // console.log('pat select change', $(this))
                if ($(this).val() == 'null') {
                    part.pattern = null
                } else {
                    part.type.availablePatterns().forEach((p) => {
                        if (p.name == $(this).val())
                            part.pattern = p;
                    })
                }
                if (part.parentPart != null)
                    part.parentPart.checkRecursive();
                else
                    part.checkRecursive();
            })
            pat.appendTo(this.uiElement)
            // $('<span>!</span>').appendTo(this.uiElement).click(() => {
            //     //TODO
            // })
            $(' ').appendTo(this.uiElement);
            $('<button class="add-part">+</button>').appendTo(this.uiElement).click(() => {
                if (part.pattern == null) {
                    $('<div>نخست یک الگو برای این بخش انتخاب کنید.</div>').dialog();
                    return ;
                }

                let d = $('<div class="add-part-subpart-type"><select id="type-select"></select></div>');
                part.pattern.availableTypes().forEach((tt) => {
                    $('<option/>').val(tt.name).text(tt.title()).appendTo(d.find('#type-select'))
                })

                d.dialog({
                    classes: {
                        "ui-dialog": "ui-corner-all add-part-dialog"
                    }, 
                    buttons: [
                        {
                        text: "قبول",
                        click: function() {
                            let st = null;
                            // console.log('selected type', part, d.find('option:selected').val())
                            part.pattern.availableTypes().forEach((tt) => {
                                if (tt.name == d.find('option:selected').val())
                                    st = tt;
                            })
                            let np = new Part(part.manager, st, part);
                            part.subparts.push(np);
                            part.container.append(np.render())
                            part.manager.applySelection(np);
                            part.checkRecursive();
                            //TODO
                            part.manager.selectToggle(np);
                            $( this ).dialog( "close" );
                        }
                        }, {
                            text: "لغو",
                            click: function() {
                            $( this ).dialog( "close" );
                            }
                        }
                    ]
                    
                })

            })
        }

        if (part.parentPart != null) {
            $('<span>&#x2715;</span>').appendTo(this.uiElement).click(() => {
                // console.log(part.parentPart)
                part.parentPart.erase(part)
            })
        }
        this.info = $('<button title="" class="info">&#9432;</button>').appendTo(this.uiElement).tooltip({
            content: function() {
                // console.log('content', part.comments.reduce((sum, c) => sum + '<li>' + c.render(), ''))
                if (part.comments.length > 0)
                    return "<ul>" + part.comments.reduce((sum, c) => sum + c.render(), '') + '</ul>';
                else   
                    return "";
            }
        });
        this.container = $('<div class="container"/>').sortable({
            beforeStop: function(event, ui) {
                // let childrenIds = part.container.children().map(e => e.attr('id'));
                let childrenIdIndexMap = part.container.children().map((i,e) => e.getAttribute('id')).toArray().reduce((obj, item, index) => ({...obj, [item]: index}), {})
                // console.log(childrenIds)
                let ret = new Array(childrenIdIndexMap.length);
                part.subparts.forEach((p) => { ret[childrenIdIndexMap[p.id]] = p; })
                part.subparts = ret;
                console.assert(JSON.stringify(part.container.children().map((i,e) => e.getAttribute('id')).toArray()) == JSON.stringify(part.subparts.map((e) => e.id)));
                // console.log(ui, ui.children(), childrenIds)
                // console.log(ui, ui.placeholder)
                // console.log(part.container.children())
                console.log(part.subparts)
                if (part.parentPart != null)
                    part.parentPart.checkRecursive();
                else
                    part.checkRecursive();
            }
        })
        this.uiElement.append(this.container)
        return this.uiElement;
    }

    dataToJSON() {
        return {
            range: this.range,
            pattern: this.pattern != null ? this.pattern.name : null,
            type: this.type != null ? this.type.name : null,
            subparts: this.subparts.map((p) => p.dataToJSON())
        }
    }

    // loadFromJSON(json) {
    //     this.range = json.range
    //     this.pattern = json.pattern.name
    // }
    
}

class TypeSure extends Type {
    static title() {
        return "سوره";
        // return "Sure";
    }
    static availablePatterns() {
        return [PatternSure_IntroStoriesConclude]
    }
}

class TypeSureIntro {
    static title() {
        return "درس سوره";
        // return "Sure Introduction";
    }
    static availablePatterns() {
        return []
    }
}

class TypeSureConclusion {
    static title() {
        return "نتیجه‌گیری سوره";
        // return "Sure Conclusion";
    }
    static availablePatterns() {
        return []
    }
}

class TypeStory {
    static title() {
        return "داستان";
        // return "Story";
    }
    static availablePatterns() {
        return []
    }

}

class TypeStoryPassage {
    static title() {
        return "پاساژ داستان با قسمت بعد";
        // return "Story Passage";
    }
    static availablePatterns() {
        return []
    }

}

class TypeStoryConclusion {
    static title() {
        return "جمع‌بندی داستان";
        // return "Story Conclusion";
    }
    static availablePatterns() {
        return [PatternStoryConclusion_FutureMap]
    }

}


// this is a patter for sure, 
class PatternSure_IntroStoriesConclude extends Pattern {

    static title() {
        return "سوره داستانی"
        // return "Stories"
    }

    static reapply(part) {
        let subparts = part.subparts;
        if (subparts.length == 0 || subparts[0].type != TypeSureIntro) {
            p = new Part(part.manager, TypeSureIntro, part);
            subparts.splice(0, 0, p)
        }

        if (!subparts.some(p => p.type == TypeStory)) {
            subparts.splice(1, 0, new Part(part.manager, TypeStory, part))
            subparts.splice(2, 0, new Part(part.manager, TypeStoryPassage, part))
            subparts.splice(3, 0, new Part(part.manager, TypeStoryConclusion, part))
        }

        if (subparts.length == 0 || subparts.slice(-1).type != TypeSureConclusion) {
            p = new Part(part.manager, TypeSureConclusion, part);
            subparts.push(p)
        }
    }

    static check(part) {
        //comments should be cleared before calling it.
        let subparts = part.subparts;
        // let retBetween = Array(subparts.length + 1).fill([TypeSureIntro.get(), TypeStory.get(), TypeStoryConclusion.get(), TypeSureConclusion.get()]);

        console.log(subparts)

        for (const [pi,p] of subparts.entries()) {
            if (pi == 0 && p.type != TypeSureIntro) {
                p.comments.push(new PartComment(PartFillingStatus.Warning, "اولین زیربخش باید " + TypeSureIntro.title() + " باشد."));
            }
            if (p.type == TypeStory) {
                let storyHasConclusion = false;
                // console.log('check', pi, pi+1, p, subparts, (pi+1) < subparts.length ? subparts[1] : '')
                if ((pi+1 < subparts.length && subparts[pi+1].type == TypeStoryConclusion) || 
                    (pi+2 < subparts.length && subparts[pi+1].type == TypeStoryPassage && subparts[pi+2].type != TypeStoryConclusion)) {
                    storyHasConclusion = true;
                }
                if (!storyHasConclusion) {
                    p.comments.push(new PartComment(PartFillingStatus.Warning, 
                        `معمولا بعد از یک داستان، آیه‌هایی نتیجه‌گیری داستان را انجام می‌دهند، در آن با رسول الله (ص) و یا مومنین آن زمان صحبت می‌شود. در این صورت می‌توانید یک ${TypeStoryConclusion.title()} بعد از این بخش اضافه کنید.`
                        ));
                }
                if (storyHasConclusion && subparts[pi+1].type != TypeStoryPassage) {
                    p.comments.push(new PartComment(PartFillingStatus.Warning, "بعد از داستان‌ها و قبل از " + TypeStoryConclusion.title() + " باید یک " + TypeStoryPassage.title() + " باشد."));
                }
            }
            if (pi == subparts.length-1 && p.type != TypeSureConclusion) {
                p.comments.push(new PartComment(PartFillingStatus.Warning, "آخرین زیربخش باشد " + TypeSureConclusion.title() + " باشد."));
            }
            if (!new Set([TypeSureIntro, TypeStory, TypeStoryPassage, TypeStoryConclusion]).has(p.type)) {
                p.comments.push(new PartComment(PartFillingStatus.Warning, "بخش حاضر نباید شامل این نوع باشد."));
            }
        }

        if (!subparts.some(p => p.type == TypeStory)) {
            part.comments.push(new PartComment(PartFillingStatus.Warning, "باید حداقل یک " + TypeStory.title() + " در این بخش باشد."));
        }
        
        if (subparts.length == 0 || subparts[0].type != TypeSureIntro) {
            part.comments.push(new PartComment(PartFillingStatus.Warning, "اولین زیربخش باشد " + TypeSureIntro.title() + " باشد."));
        }

        // if (subparts.length == 0 || subparts.slice(-1).type != TypeSureConclusion) {
        //     part.comments.push(new PartComment(PartFillingStatus.Warning, "Last element should be " + TypeSureConclusion.title()));
        // }
    }

    static availableTypes() {
        return [TypeSureIntro, TypeStory, TypeStoryConclusion, TypeSureConclusion];
    }

}


class TypeStoryConclusionConclusion {
    static title() {
        return "جمع‌بندی داستان";
        // return "Story Conclusion Conslusion";
    }
    static availablePatterns() {
        return []
    }

}

class TypeStoryConclusionNowFuture {
    static title() {
        return "حال و تصویر آینده";
        // return "Story Conclusion Now Future";
    }
    static availablePatterns() {
        return [PatternStoryConclusionNowFuture_1]
    }

}

// this is a patter for StoryConclusion, 
class PatternStoryConclusion_FutureMap extends Pattern {

    static title() {
        // return "Now Futures"
        return "حال و آینده"
    }

    static reapply(part) {
        let subparts = part.subparts;

        if (!subparts.some(p => p.type == TypeStoryConclusionNowFuture)) {
            subparts.push(new Part(part.manager, TypeStoryConclusionNowFuture, part))
        }

        if (subparts.length == 0 || subparts.slice(-1).type != TypeStoryConclusionConclusion) {
            p = new Part(part.manager, TypeStoryConclusionConclusion, part);
            subparts.push(p);
        }
    }

    static check(part) {
        //comments should be cleared before calling it.
        let subparts = part.subparts;
        // let retBetween = Array(subparts.length + 1).fill([TypeSureIntro.get(), TypeStory.get(), TypeStoryConclusion.get(), TypeSureConclusion.get()]);

        for (const pi in subparts) {
            
            if (!new Set([TypeStoryConclusionConclusion, TypeStoryConclusionNowFuture]).has(subparts[pi].type)) {
                subparts[pi].comments.push(new PartComment(PartFillingStatus.Warning, "بخش حاضر شامل این نوع نمی‌شود."));
            }
            if (pi != subparts.length-1 && subparts[pi].type == TypeStoryConclusionConclusion) {
                subparts[pi].comments.push(new PartComment(PartFillingStatus.Warning, `این زیربخش باید آخرین زیربخش بخش حاضر باشد.`));
            }
            if (pi == subparts.length-1 && subparts[pi].type != TypeStoryConclusionConclusion) {
                subparts[pi].comments.push(new PartComment(PartFillingStatus.Warning, `آخرین زیربخش باید${TypeStoryConclusionConclusion.title()} باشد.`));
            }
        }

        if (!subparts.some(p => p.type == TypeStoryConclusionConclusion)) {
            part.comments.push(new PartComment(PartFillingStatus.Warning, `باید حداقل یک ${TypeStoryConclusionConclusion.title() } در بخش حاضر باشد.`));
        }
        
    }

    static availableTypes() {
        return [TypeStoryConclusionNowFuture, TypeStoryConclusionConclusion];
    }


}



class TypeScene {
    static title() {
        return "صحنه";
        // return "Now Future Line";
    }
    static availablePatterns() {
        return []
    }

}

// this is a patter for StoryConclusion, 
class PatternStoryConclusionNowFuture_1 extends Pattern {

    static title() {
        // return "Now Future"
        return "صحنه‌ها"
    }

    static reapply(part) {
        let subparts = part.subparts;

        if (!subparts.some(p => p.type == TypeScene)) {
            subparts.push(new Part(part.manager, TypeScene, part))
        }
    }

    static check(part) {
        //comments should be cleared before calling it.
        let subparts = part.subparts;
        // let retBetween = Array(subparts.length + 1).fill([TypeSureIntro.get(), TypeStory.get(), TypeStoryConclusion.get(), TypeSureConclusion.get()]);

        for (const pi in subparts) {
            
            if (!new Set([TypeScene]).has(subparts[pi].type)) {
                subparts[pi].comments.push(new PartComment(PartFillingStatus.Warning, "زیربخشی از این نوع نمی‌تواند در این بخش باشد."));
            }
        }

    }

    static availableTypes() {
        return [TypeScene];
    }


}

