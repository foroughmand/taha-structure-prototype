# Structure finding of Taha Sure

## Name
A prototype for structure finding application of Taha Sure

## Description
In this project, user finds parts and sub-parts of the sure with the helps given by the application.

## Visuals
![Screenshot 01](doc/screenshot-01.png?raw=true "Screenshot")


## Usage
[http://qstruct.rf.gd/](http://qstruct.rf.gd/)

## Support
No support is available yet.

## Roadmap
* Extend to other sure
* Save result
* Load result
* Multilanguage
* Move evaluation

## Contributing
Contribution is welcome.

## License
This project is under MIT licence.

## Project status
Project is a demo.
